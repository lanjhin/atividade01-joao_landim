package br.ucsal;

import javax.swing.JOptionPane;

public class Questao03 {

	public static void main(String[] args) {
		int nota = Integer
				.parseInt(JOptionPane.showInputDialog(null, "Informe a nota: ", "NOTA", JOptionPane.QUESTION_MESSAGE));

		if (nota >= 0 && nota <= 49) {
			JOptionPane.showMessageDialog(null, "Conceito da nota: Insuficiente", "CONCEITO",
					JOptionPane.WARNING_MESSAGE);

		}
		if (nota >= 50 && nota <= 64) {
			JOptionPane.showMessageDialog(null, "Conceito da nota: Regular", "CONCEITO", JOptionPane.WARNING_MESSAGE);

		}
		if (nota >= 65 && nota <= 84) {
			JOptionPane.showMessageDialog(null, "Conceito da nota: Bom", "CONCEITO", JOptionPane.INFORMATION_MESSAGE);

		}
		if (nota >= 85 && nota <= 100) {
			JOptionPane.showMessageDialog(null, "Conceito da nota: Otimo", "CONCEITO", JOptionPane.INFORMATION_MESSAGE);

		}
	}

}
