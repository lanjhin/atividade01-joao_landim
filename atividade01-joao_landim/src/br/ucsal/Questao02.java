package br.ucsal;

import javax.swing.JOptionPane;

public class Questao02 {

	public static void main(String[] args) {
		int num1 = Integer.parseInt(JOptionPane.showInputDialog(null, "Informe um numero inteiro: ", "NUMERO 1",
				JOptionPane.QUESTION_MESSAGE));
		int num2 = Integer.parseInt(JOptionPane.showInputDialog(null, "Informe um numero inteiro novamente: ",
				"NUMERO 2", JOptionPane.QUESTION_MESSAGE));
		int soma = num1 + num2;

		JOptionPane.showMessageDialog(null, "Resultado da soma entre os dois numeros: " + soma, "RESULTADO",
				JOptionPane.INFORMATION_MESSAGE);

	}

}
