package br.ucsal;

import java.util.*;

public class Questao06 {

	public static void main(String[] args) {
		Scanner sc = new Scanner(System.in);

		int i = 0;
		int soma = 0;
		int vet1[] = new int[10];
		int vet2[] = new int[10];
		int vet3[] = new int[10];

		// Entrada de dados
		for (i = 0; i < vet1.length; i++) {
			System.out.println("Digite dez numeros aleatorios do primeiro vetor: ");
			vet1[i] = sc.nextInt();
		}

		for (i = 0; i < vet2.length; i++) {
			System.out.println("Digite dez numeros aleatorios do segundo vetor: ");
			vet2[i] = sc.nextInt();
		}

		// Processamento de dados
		for (i = 0; i < vet3.length; i++) {
			vet3 = vet1;
		}

		for (i = 0; i < vet3.length; i++) {
			vet3 = vet2;
			soma += vet1[i] + vet2[i];
		}

		// Saida de dados
		System.out.println(soma);
	}
}