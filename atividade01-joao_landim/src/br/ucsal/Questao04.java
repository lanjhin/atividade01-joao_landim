package br.ucsal;

import javax.swing.JOptionPane;

public class Questao04 {

	public static void main(String[] args) {
		String letra = JOptionPane.showInputDialog(null, "Informe uma letra do alfabeto: ", "",
				JOptionPane.QUESTION_MESSAGE);

		if (letra.equalsIgnoreCase("A") || letra.equalsIgnoreCase("E") || letra.equalsIgnoreCase("I")
				|| letra.equalsIgnoreCase("O") || letra.equalsIgnoreCase("U")) {
			JOptionPane.showMessageDialog(null, "A letra informada e uma vogal!", "INFORMACAO",
					JOptionPane.INFORMATION_MESSAGE);

		} else {
			JOptionPane.showMessageDialog(null, "A letra informada e uma consoante", "INFORMACAO",
					JOptionPane.INFORMATION_MESSAGE);
		}
	}

}
