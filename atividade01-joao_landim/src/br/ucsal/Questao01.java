package br.ucsal;

import javax.swing.JOptionPane;

public class Questao01 {

	public static void main(String[] args) {
		String nome = JOptionPane.showInputDialog(null, "Informe o seu nome: ", "NOME", JOptionPane.QUESTION_MESSAGE);
		String sobrenome = JOptionPane.showInputDialog(null, "Informe o seu sobrenome: ", "SOBRENOME",
				JOptionPane.QUESTION_MESSAGE);
		String nomeCompleto = nome + " " + sobrenome;

		JOptionPane.showMessageDialog(null, "Nome completo: " + nomeCompleto, "INFORMACAO",
				JOptionPane.INFORMATION_MESSAGE);
	}

}
